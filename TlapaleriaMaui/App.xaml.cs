﻿//using Android.Content.Res;
using TlapaleriaMaui.Pages;

namespace TlapaleriaMaui
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new AppShell();
            MainPage = new NavigationPage(new LoginPage());
        }
    }
}