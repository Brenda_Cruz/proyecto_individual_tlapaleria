﻿using COMMON.Modelos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TlapaleriaMaui.Services
{
    public class LoginServices:ILoginRepository
    {
        public async Task<Login> Login(string idusuario, string password)
        {
            var Login = new List<Login>();
            var client = new HttpClient();

            //192.168.167.110
            //string url = $"http://bren-api.somee.com/api/Usuarios/LoginUsuarios/{idusuario}/{password}";
            
            string url = $"http://bren-api.somee.com/api/LoginUsuario/{idusuario}/{password}";
            
            client.BaseAddress = new Uri(url);
            HttpResponseMessage response = await client.GetAsync("");

            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                Login = JsonConvert.DeserializeObject<List<Login>>(content);
                return await Task.FromResult(Login.FirstOrDefault());
            }
            else
            {
                return null;

            }
        }
    }
}