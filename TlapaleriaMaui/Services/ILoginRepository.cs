﻿using COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TlapaleriaMaui.Services
{
    public interface ILoginRepository
    {
        Task<Login> Login(string idusuario, string password);
    }
}
