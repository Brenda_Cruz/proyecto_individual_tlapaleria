using BIZ.Managers;
using BIZ;
using COMMON.Entidades;
using COMMON.Modelos;

namespace TlapaleriaMaui.Pages;

public partial class CatalogoUsuarios : ContentPage
{
    UsuariosManager usuariosManager;

    public CatalogoUsuarios()
    {
        InitializeComponent();
        usuariosManager = FabricManager.UsuariosManager();
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstUsuarios.ItemsSource = null;
        List<ModelUsuarioListado> listados = usuariosManager.ObtenerTodosListados();
        if (listados != null)
        {
            lstUsuarios.ItemsSource = listados;
        }
        else
        {
            DisplayAlert("Error", "Verifica tu conexi�n a Internet", "Ok");
        }
    }

    private void btnNuevoUsuario_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarUsuarios(new COMMON.Entidades.Usuarios()));
    }

    private void lstUsuarios_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        Navigation.PushAsync(new EditarUsuarios(e.SelectedItem as Usuarios));
    }

}