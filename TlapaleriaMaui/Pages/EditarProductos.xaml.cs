using BIZ.Managers;
using BIZ;
using COMMON.Entidades;

namespace TlapaleriaMaui.Pages;

public partial class EditarProductos : ContentPage
{
    Productos productos;
    ProductosManager productosManager;
    MarcaManager marcaManager;
    CategoriasManager categoriasManager;
    bool esNuevo;
    public EditarProductos(Productos productos)
    {
        InitializeComponent();
        this.BindingContext = productos;
        this.productos = productos;
        esNuevo = string.IsNullOrEmpty(productos.Id);
        productosManager = FabricManager.ProductosManager();

        marcaManager = FabricManager.MarcaManager();
        categoriasManager = FabricManager.CategoriasManager();

        List<Marca> marcas = marcaManager.ObtenerTodos;
        pkrMarca.ItemsSource = marcas;

        List<Categorias> categorias = categoriasManager.ObtenerTodos;
        pkrCategoria.ItemsSource = categorias;

        btnEliminar.IsVisible = !esNuevo;
        if (!esNuevo)
        {
            pkrMarca.SelectedItem = marcas.Where(r => r.Id == productos.IdMarca).SingleOrDefault();
            pkrCategoria.SelectedItem = categorias.Where(r => r.Id == productos.IdCategoria).SingleOrDefault();
            if (productos.FechaIngreso != null)
            {
                dpFechaHora.Date = productos.FechaIngreso.Value;
                tpHora.Time = productos.FechaIngreso.Value.TimeOfDay;
            }
        }
        else
        {
            dpFechaHora.Date = DateTime.Now;
            tpHora.Time = DateTime.Now.TimeOfDay;
            btnEliminar.IsVisible = false;
        }
        BindingContext = productos;

    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        Productos u = this.BindingContext as Productos;
        Marca r = pkrMarca.SelectedItem as Marca;
        Categorias j
            = pkrCategoria.SelectedItem as Categorias;
        u.IdMarca = r.Id;
        u.IdCategoria = j.Id;

        //Guardar
        Productos result = esNuevo ? productosManager.Insertar(u) : productosManager.Modificar(u, u.Id);
            if (result != null)
            {
                DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
                Navigation.PopAsync();
            }
            else
            {
                DisplayAlert("Error", productosManager.Error, "Ok");
            }

    }

    private async void btnEliminar_Clicked(object sender, EventArgs e)
    {
        try
        {
            var r = await DisplayAlert("Confirma", "�Estas seguro de eliminar este registro?", "Si", "No");
            if (r)
            {
                if (productosManager.Eliminar(productos.Id))
                {
                    await DisplayAlert("�xito", "Registro eliminado correctamente", "Ok");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", productosManager.Error, "Ok");
                }

            }
        }
        catch (Exception ex)
        {
            await DisplayAlert("Error", ex.Message, "Ok");
        }
    }
}