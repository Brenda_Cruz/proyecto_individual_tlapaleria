//using Android.Webkit;
using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace TlapaleriaMaui.Pages;

public partial class EditarMarcas : ContentPage
{
    Marca marca;
    MarcaManager marcaManager;
    bool esNuevo;
    public EditarMarcas(Marca marca)
	{
		InitializeComponent();
        this.BindingContext = marca;
        this.marca = marca;
        esNuevo = string.IsNullOrEmpty(marca.Id);
        marcaManager = FabricManager.MarcaManager();

        btnEliminar.IsVisible = !esNuevo;
    }

    private async void btnEliminar_Clicked(object sender, EventArgs e)
    {
        try
        {
            var r=await DisplayAlert("Confirma", "�Esta seguro de eliminar este registro?", "Si", "No");

            if (r)
            {
                if (marcaManager.Eliminar(marca.Id))
                {
                    await DisplayAlert("�xito", "Registro eliminado correctamente", "Ok");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", marcaManager.Error, "Ok");
                }

            }
        }
        catch (Exception ex)
        {
            await DisplayAlert("Error", ex.Message, "Ok");
        }
    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        Marca u = this.BindingContext as Marca;

        //Guardar
        Marca result = esNuevo ? marcaManager.Insertar(u) : marcaManager.Modificar(u, u.Id);
        if (result != null)
        {
            DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
            Navigation.PopAsync();
        }
        else
        {
            DisplayAlert("Error", marcaManager.Error, "Ok");
        }

    }
}