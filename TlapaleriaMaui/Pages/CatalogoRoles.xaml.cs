using BIZ;
using BIZ.Managers;
using COMMON.Entidades;
using COMMON.Modelos;

namespace TlapaleriaMaui.Pages;

public partial class CatalogoRoles : ContentPage
{
    RolManager rolManager;
	public CatalogoRoles()
	{
		InitializeComponent();
        rolManager = FabricManager.RolManager();
	}

    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstRoles.ItemsSource = null;
        List<ModelRolListado> listados = rolManager.ObtenerTodosListados();
        if (listados != null)
        {
            lstRoles.ItemsSource = listados;
        }
        else
        {
            DisplayAlert("Error", "Verifica tu conexi�n a Internet", "Ok");
        }
    }

    private void lstRoles_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        Navigation.PushAsync(new EditarRoles(e.SelectedItem as Rol));
    }


    private void btnNuevoRol_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarRoles(new COMMON.Entidades.Rol()));
    }
}