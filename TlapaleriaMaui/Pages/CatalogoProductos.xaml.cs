using BIZ.Managers;
using BIZ;
using COMMON.Entidades;
using COMMON.Modelos;

namespace TlapaleriaMaui.Pages;

public partial class CatalogoProductos : ContentPage
{
    ProductosManager productosManager;
    //List<Productos> productos;
    List<Marca> marcas;
    List<Categorias> categorias;
    public CatalogoProductos()
    {
        InitializeComponent();
        productosManager = FabricManager.ProductosManager();
   
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstProductos.ItemsSource = null;
        List<ModelProductosListado> listados = productosManager.ObtenerTodosListados();
        if (listados != null)
        {
            lstProductos.ItemsSource = listados;
        }
        else
        {
            DisplayAlert("Error", "Verifica tu conexi�n a Internet", "Ok");
        }
    }

    private void btnNuevoProductos_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarProductos(new COMMON.Entidades.Productos()));
    }

    private void lstProductos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        Navigation.PushAsync(new EditarProductos(e.SelectedItem as Productos));
    }
}