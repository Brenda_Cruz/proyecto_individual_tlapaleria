using TlapaleriaMaui.Services;
using COMMON.Modelos;
using COMMON.Entidades;


namespace TlapaleriaMaui.Pages;

public partial class LoginPage : ContentPage
{
    readonly ILoginRepository _loginRepository = new LoginServices();
    public LoginPage()
	{
		InitializeComponent();
	}

    //private async void btnLogin_Clicked(object sender, EventArgs e)
    //{

    //}

    private async void btnLogin_Clicked_1(object sender, EventArgs e)
    {
        string idusuario = txtUsuario.Text;
        string password = txtPassword.Text;

        if (idusuario == null || password == null)
        {
            await DisplayAlert("Error", "deve de ingresar tant Usuario como Contrasea para poder ingresar", "Ok");
            return;
        }


        Login login = await _loginRepository.Login(idusuario, password);
        if (login == null)
        {
            Application.Current.MainPage = new AppShell();
            //await DisplayAlert("Exito",$"Bienvenido {usuario.Nombre}","Ok");
        }
        else
        {
            await DisplayAlert("Error", "Usuario o Contrasea Incorrectos", "Ok");
        }


    }
}