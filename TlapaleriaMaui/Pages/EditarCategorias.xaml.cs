using BIZ;
using BIZ.Managers;
using COMMON.Entidades;
//using Windows.ApplicationModel.Chat;

namespace TlapaleriaMaui.Pages;

public partial class EditarCategorias : ContentPage
{
    Categorias categorias;
    CategoriasManager categoriasManager;
    bool esNuevo;

	public EditarCategorias(Categorias categorias)
	{
		InitializeComponent();
        this.BindingContext = categorias;
        this.categorias = categorias;
        esNuevo = string.IsNullOrEmpty(categorias.Id);
        categoriasManager = FabricManager.CategoriasManager();

        btnEliminar.IsVisible = !esNuevo;
    }

    private async void btnEliminar_Clicked(object sender, EventArgs e)
    {
        try
        {
            var r=await DisplayAlert("Confirma", "�Esta seguro de eliminar este registro?", "Si", "No");

            if (r)
            {
                if (categoriasManager.Eliminar(categorias.Id))
                {
                    await DisplayAlert("�xito", "Registro eliminado correctamente", "Ok");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", categoriasManager.Error, "Ok");
                }

            }
        }
        catch (Exception ex)
        {
            await DisplayAlert("Error", ex.Message, "Ok");
        }
    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        Categorias u = this.BindingContext as Categorias;

        //Guardar
        Categorias result = esNuevo ? categoriasManager.Insertar(u) : categoriasManager.Modificar(u, u.Id);
        if (result != null)
        {
            DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
            Navigation.PopAsync();
        }
        else
        {
            DisplayAlert("Error", categoriasManager.Error, "Ok");
        }

    }
}