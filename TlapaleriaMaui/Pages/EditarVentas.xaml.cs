using BIZ.Managers;
using BIZ;
using COMMON.Entidades;

namespace TlapaleriaMaui.Pages;

public partial class EditarVentas : ContentPage
{
    Ventas ventas;
    VentasManager ventasManager;
    ProductosManager productosManager;
    ClientesManager clientesManager;
    UsuariosManager usuariosManager;
    bool esNuevo;
    public EditarVentas(Ventas ventas)
    {
        InitializeComponent();
        this.BindingContext = ventas;
        this.ventas = ventas;
        esNuevo = string.IsNullOrEmpty(ventas.Id);
        ventasManager = FabricManager.VentasManager();

        productosManager = FabricManager.ProductosManager();
        usuariosManager = FabricManager.UsuariosManager();
        clientesManager = FabricManager.ClientesManager();

        List<Productos> productos = productosManager.ObtenerTodos;
        List<Usuarios> usuarios = usuariosManager.ObtenerTodos;
        List<Clientes> clientes = clientesManager.ObtenerTodos;

        pkrIdProducto.ItemsSource = productos;
        pkrIdUsuario.ItemsSource = usuarios;
        pkrIdCliente.ItemsSource = clientes;

        btnEliminar.IsVisible = !esNuevo;
        if (!esNuevo)
        {
            pkrIdProducto.SelectedItem = productos.Where(r => r.Id == ventas.IdProducto).SingleOrDefault();
            pkrIdUsuario.SelectedItem = usuarios.Where(r => r.Id == ventas.IdUsuario).SingleOrDefault();
            pkrIdCliente.SelectedItem = clientes.Where(r => r.Id == ventas.IdCliente).SingleOrDefault();

            if (ventas.FechaVenta != null)
            {
                dpFechaHora.Date = ventas.FechaVenta.Value;
                tpHora.Time = ventas.FechaVenta.Value.TimeOfDay;
            }
        }
        else
        {
            dpFechaHora.Date = DateTime.Now;
            tpHora.Time = DateTime.Now.TimeOfDay;
            btnEliminar.IsVisible = false;
        }
        BindingContext = ventas;

    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        Ventas u = this.BindingContext as Ventas;
        Productos r = pkrIdProducto.SelectedItem as Productos;
        Usuarios o = pkrIdUsuario.SelectedItem as Usuarios;
        Clientes c = pkrIdCliente.SelectedItem as Clientes;
        u.IdProducto = r.Id;
        u.IdUsuario = o.Id;
        u.IdCliente = c.Id;
        //u.Habilitado = true;

        u.FechaVenta = dpFechaHora.Date;
        DateTime fecha = dpFechaHora.Date;
        TimeSpan hora = tpHora.Time;
        ventas.FechaVenta = new DateTime(fecha.Year, fecha.Month, fecha.Day, hora.Hours, hora.Minutes, hora.Seconds);

        //Guardar
        Ventas result = esNuevo ? ventasManager.Insertar(u) : ventasManager.Modificar(u, u.Id);
            if (result != null)
            {
                DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
                Navigation.PopAsync();
            }
            else
            {
                DisplayAlert("Error", ventasManager.Error, "Ok");
            }
       
    }

    private async void btnEliminar_Clicked(object sender, EventArgs e)
    {
        try
        {
            var r = await DisplayAlert("Confirma", "�Estas seguro de eliminar este registro?", "Si", "No");
            if (r)
            {
                if (ventasManager.Eliminar(ventas.Id))
                {
                    await DisplayAlert("�xito", "Registro eliminado correctamente", "Ok");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", ventasManager.Error, "Ok");
                }

            }
        }
        catch (Exception ex)
        {
            await DisplayAlert("Error", ex.Message, "Ok");
        }

    }
}