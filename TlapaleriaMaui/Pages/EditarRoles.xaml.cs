using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace TlapaleriaMaui.Pages;

public partial class EditarRoles : ContentPage
{
    Rol rol;
    RolManager rolManager;
    bool esNuevo;

	public EditarRoles(Rol rol)
	{
		InitializeComponent();
        this.BindingContext = rol;
        this.rol = rol;
        esNuevo = string.IsNullOrEmpty(rol.Id);
        rolManager = FabricManager.RolManager();

        btnEliminar.IsVisible = !esNuevo;
    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        Rol u = this.BindingContext as Rol;

        // u.EsAdministrador = true;
        if (u.EsAdministrador)
        {
            u.EsAdministrador = true;
        }
        else
        {
            Console.WriteLine("No es administrador");
        }
        //Guardar
        Rol result = esNuevo ? rolManager.Insertar(u) : rolManager.Modificar(u, u.Id);
        if (result != null)
        {
            DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
            Navigation.PopAsync();
        }
        else
        {
            DisplayAlert("Error", rolManager.Error, "Ok");
        }

    }

    private async void btnEliminar_Clicked(object sender, EventArgs e)
    {
        try
        {
            var r = await DisplayAlert("Confirma", "�Esta seguro de eliminar este registro?", "Si", "No");

            if (r)
            {
                if (rolManager.Eliminar(rol.Id))
                {
                    await DisplayAlert("�xito", "Registro eliminado correctamente", "Ok");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", rolManager.Error, "Ok");
                }

            }
        }
        catch (Exception ex)
        {
            await DisplayAlert("Error", ex.Message, "Ok");
        }
    }
}