
using BIZ.Managers;
using BIZ;
using COMMON.Entidades;

namespace TlapaleriaMaui.Pages;

public partial class EditarClientes : ContentPage
{
    Clientes clientes;
    ClientesManager clientesManager;
    //RolManager rolManager;
    bool esNuevo;
    public EditarClientes(Clientes clientes)
    {
        InitializeComponent();
        this.BindingContext = clientes;
        this.clientes = clientes;
        esNuevo = string.IsNullOrEmpty(clientes.Id);
        clientesManager = FabricManager.ClientesManager();
        //rolManager = FabricManager.RolManager();
        //List<Rol> roles = rolManager.ObtenerTodos;
        //pkrRol.ItemsSource = roles;
        btnEliminar.IsVisible = !esNuevo;
        //if (!esNuevo)
        //{
          //  pkrRol.SelectedItem = roles.Where(r => r.Id == clientes.IdRol).SingleOrDefault();
        //}

    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        Clientes u = this.BindingContext as Clientes;
        //Rol r = pkrRol.SelectedItem as Rol;
        //u.IdRol = r.Id;
        u.Habilitado = true;
        if (u.Password == entPassword2.Text)
        {
            //Guardar
            Clientes result = esNuevo ? clientesManager.Insertar(u) : clientesManager.Modificar(u, u.Id);
            if (result != null)
            {
                DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
                Navigation.PopAsync();
            }
            else
            {
                DisplayAlert("Error", clientesManager.Error, "Ok");
            }
        }
        else
        {
            DisplayAlert("Error", "Las contrase�as no son iguales", "Ok");
        }
    }

    private async void btnEliminar_Clicked(object sender, EventArgs e)
    {
        try
        {
            var r = await DisplayAlert("Confirma", "�Estas seguro de eliminar este registro?", "Si", "No");
            if (r)
            {
                if (clientesManager.Eliminar(clientes.Id))
                {
                    await DisplayAlert("�xito", "Registro eliminado correctamente", "Ok");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", clientesManager.Error, "Ok");
                }

            }
        }
        catch (Exception ex)
        {
            await DisplayAlert("Error", ex.Message, "Ok");
        }

    }
}