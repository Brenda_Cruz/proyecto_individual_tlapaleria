using BIZ.Managers;
using BIZ;
using COMMON.Entidades;
using COMMON.Modelos;

namespace TlapaleriaMaui.Pages;

public partial class CatalogoClientes : ContentPage
{
    ClientesManager clientesManager;
    List<Clientes> clienres;
    public CatalogoClientes()
    {
        InitializeComponent();
        clientesManager = FabricManager.ClientesManager();
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstClientes.ItemsSource = null;
        List<ModelClientesListado> listados = clientesManager.ObtenerTodosListados();
        if (listados != null)
        {
            lstClientes.ItemsSource = listados;
        }
        else
        {
            DisplayAlert("Error", "Verifica tu conexi�n a Internet", "Ok");
        }
    }

    private void lstClientes_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        Navigation.PushAsync(new EditarClientes(e.SelectedItem as Clientes));
    }

    private void btnNuevoCliente_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarClientes(new COMMON.Entidades.Clientes()));
    }
}