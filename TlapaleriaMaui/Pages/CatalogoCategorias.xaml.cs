using BIZ;
using BIZ.Managers;
using COMMON.Entidades;
using COMMON.Modelos;
//using Windows.ApplicationModel.Chat;

namespace TlapaleriaMaui.Pages;

public partial class CatalogoCategorias : ContentPage
{
    CategoriasManager categoriasManager;

	public CatalogoCategorias()
	{
		InitializeComponent();
        categoriasManager = FabricManager.CategoriasManager();
	}

    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstCategorias.ItemsSource = null;
        List<ModelCategoriasListado> listados = categoriasManager.ObtenerTodosListados();
        if (listados != null)
        {
            lstCategorias.ItemsSource = listados;
        }
        else
        {
            DisplayAlert("Error", "Verifica tu conexi�n a Internet", "Ok");
        }
    }

    private void btnNuevaCategoria_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarCategorias(new COMMON.Entidades.Categorias()));
    }

    private void lstCategorias_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        Navigation.PushAsync(new EditarCategorias(e.SelectedItem as Categorias));
    }
}