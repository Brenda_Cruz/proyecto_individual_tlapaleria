using BIZ;
using BIZ.Managers;
using COMMON.Entidades;
using COMMON.Modelos;

namespace TlapaleriaMaui.Pages;

public partial class CatalogoMarcas : ContentPage
{
    MarcaManager marcaManager;
	public CatalogoMarcas()
	{
		InitializeComponent();
        marcaManager = FabricManager.MarcaManager();
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstMarcas.ItemsSource = null;
        List<ModelMarcaListado> listados = marcaManager.ObtenerTodosListados();
        if (listados != null)
        {
            lstMarcas.ItemsSource = listados;
        }
        else
        {
            DisplayAlert("Error", "Verifica tu conexi�n a Internet", "Ok");
        }
    }

    private void lstMarcas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        Navigation.PushAsync(new EditarMarcas(e.SelectedItem as Marca));
    }

    private void btnNuevaMarcas_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarMarcas(new COMMON.Entidades.Marca()));
    }
}