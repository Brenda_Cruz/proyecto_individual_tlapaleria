using BIZ.Managers;
using BIZ;
using COMMON.Entidades;
using COMMON.Modelos;
namespace TlapaleriaMaui.Pages;

public partial class CatalogoVentas : ContentPage
{
    VentasManager ventasManager;
    List<Productos> productos;
    List<Clientes> clientes;
    List<Usuarios> usuarios;

    public CatalogoVentas()
    {
        InitializeComponent();
        ventasManager = FabricManager.VentasManager();
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstVentas.ItemsSource = null;
        List<ModelVentasListado> listados = ventasManager.ObtenerTodosListados();
        if (listados != null)
        {
            lstVentas.ItemsSource = listados;
        }
        else
        {
            DisplayAlert("Error", "Verifica tu conexi�n a Internet", "Ok");
        }
    }

    private void btnNuevoVentas_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarVentas(new COMMON.Entidades.Ventas()));
    }

    private void lstVentas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        Navigation.PushAsync(new EditarVentas(e.SelectedItem as Ventas));
    }
}