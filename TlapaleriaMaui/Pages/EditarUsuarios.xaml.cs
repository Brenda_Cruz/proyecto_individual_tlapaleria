
using BIZ.Managers;
using BIZ;
using COMMON.Entidades;

namespace TlapaleriaMaui.Pages;

public partial class EditarUsuarios : ContentPage
{
    Usuarios usuario;
    UsuariosManager usuariosManager;
    RolManager rolManager;
    bool esNuevo;

    public EditarUsuarios(Usuarios usuario)
    {
        InitializeComponent();
        this.BindingContext = usuario;
        this.usuario = usuario;
        esNuevo = string.IsNullOrEmpty(usuario.Id);
        usuariosManager = FabricManager.UsuariosManager();

        rolManager = FabricManager.RolManager();

        List<Rol> roles = rolManager.ObtenerTodos;
        pkrRol.ItemsSource = roles;

        btnEliminar.IsVisible = !esNuevo;
        if (!esNuevo)
        {
            pkrRol.SelectedItem = roles.Where(r => r.Id == usuario.IdRol).SingleOrDefault();
        }
    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        Usuarios u = this.BindingContext as Usuarios;
        Rol r = pkrRol.SelectedItem as Rol;
        
        // u.Habilitado = true;
        if (u.Habilitado)
        {
            u.Habilitado = true;
        }
        else
        {
            Console.WriteLine("No es administrador");
        }

        u.IdRol = r.Id;

        if (u.Password == entPassword2.Text)
        {
            //Guardar
            Usuarios result = esNuevo ? usuariosManager.Insertar(u) : usuariosManager.Modificar(u, u.Id);
            if (result != null)
            {
                DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
                Navigation.PopAsync();
            }
            else
            {
                DisplayAlert("Error", usuariosManager.Error, "Ok");
            }
        }
        else
        {
            DisplayAlert("Error", "Las contrase�as no son iguales", "Ok");
        }
    }

    private async void btnEliminar_Clicked(object sender, EventArgs e)
    {
        try
        {
            var r = await DisplayAlert("Confirma", "�Estas seguro de eliminar este registro?", "Si", "No");
            if (r)
            {
                if (usuariosManager.Eliminar(usuario.Id))
                {
                    await DisplayAlert("�xito", "Registro eliminado correctamente", "Ok");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", usuariosManager.Error, "Ok");
                }

            }
        }
        catch (Exception ex)
        {
            await DisplayAlert("Error", ex.Message, "Ok");
        }
    }


}