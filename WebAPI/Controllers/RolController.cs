﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolController : GenericController<Rol>
    {
        public RolController() : base(new RolValidator())
        {

        }


        //-----------------------------------------------------------------------------------------------------
        [HttpGet("ObtenerRolesCompletos")]
        public ActionResult<List<ModelRolListado>> ObtenerRolesCompletos()
        {
            List<Rol> rols = ObtenerDatos<Rol>();

            List<ModelRolListado> listados = new List<ModelRolListado>();

            foreach (var item in rols)
            {
                listados.Add(new ModelRolListado()
                {
                    Id = item.Id,
                    Nombre=item.Nombre,
                    EsAdministrador = item.EsAdministrador,
                    Imagen = ObtenNombreImagen(item.Id, rols),
                    NombreRol = rols.SingleOrDefault(c => c.Id == item.Id)?.Nombre,
                    Color = item.EsAdministrador ? "#00FF00" : "#FF0000" //RGB
                });
            }

            return Ok(listados);
        }

        private string ObtenNombreImagen(string id, List<Rol> rols)
        {
            Rol r = rols.SingleOrDefault(i => i.Id == id);
            if (r != null)
            {
                switch (r.Nombre)
                {
                    case "Empleado":
                        return "empleado.png";
                    case "Encargado":
                        return "encargado.png";
                    default:
                        return "user.png";
                }
            }
            else
            {
                return "no.png";
            }
        }

    }
}
