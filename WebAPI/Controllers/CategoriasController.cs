﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriasController : GenericController<Categorias>
    {
        public CategoriasController() : base(new CategoriasValidator())
        {

        }

        //-----------------------------------------------------------------------------------------------------
        [HttpGet("ObtenerCategoriasCompletos")]
        public ActionResult<List<ModelCategoriasListado>> ObtenerCategoriasCompletos()
        {
            List<Categorias> categorias = ObtenerDatos<Categorias>();

            List<ModelCategoriasListado> listados = new List<ModelCategoriasListado>();

            foreach (var item in categorias)
            {
                listados.Add(new ModelCategoriasListado()
                {
                    Id = item.Id,
                    Nombre = item.Nombre,
                    Imagen = ObtenNombreImagen(item.Id, categorias)
                });
            }

            return Ok(listados);
        }

        private string ObtenNombreImagen(string id, List<Categorias> categorias)
        {
            Categorias r = categorias.SingleOrDefault(i => i.Id == id);
            if (r != null)
            {
                switch (r.Nombre)
                {
                    case "Jardineria":
                        return "jardineria.png";
                    case "Herramienta":
                        return "herramientas.png";
                    case "Acabados y Construccion":
                        return "construccion.png";
                    case "Plomeria":
                        return "plomeria.png";
                    case "Elictrico":
                        return "electrico.png";
                    case "Ferreteria":
                        return "maquinas.png";
                    case "Maquinaria Y Equipos Ligeros":
                        return "maquinaria.png";
                    default:
                        return "trabajos.png";
                }
            }
            else
            {
                return "no.png";
            }
        }


    }
}
