﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using COMMON.Entidades;
using COMMON.Validadores;
using COMMON.Modelos;
using LiteDB;
using System.Data;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductosController : GenericController<Productos>
    {
        public ProductosController() : base(new ProductosValidator())
        {
        }


        //-----------------------------------------------------------------------------------------------------
        [HttpGet("ObtenerProductosCompletos")]
        public ActionResult<List<ModelProductosListado>> ObtenerProductosCompletos()
        {
            List<Productos> productos = ObtenerDatos<Productos>();

            List<ModelProductosListado> listados = new List<ModelProductosListado>();

            List<Marca> marcas = ObtenerDatos<Marca>();
            List<Categorias> categorias = ObtenerDatos<Categorias>();
            foreach (var item in productos)
            {
                listados.Add(new ModelProductosListado()
                {
                    Id = item.Id,
                    IdProducto = item.IdProducto,
                    Nombre = item.Nombre,
                    Costo = item.Costo,
                    Cantidad = item.Cantidad,
                    FechaIngreso = item.FechaIngreso,
                    IdMarca = item.IdMarca,
                    IdCategoria = item.IdCategoria,
                    NombreMarcas = marcas.SingleOrDefault(c => c.Id == item.IdMarca)?.Nombre,
                    NombreCategorias = categorias.SingleOrDefault(c => c.Id == item.IdCategoria)?.Nombre
                });
            }

            return Ok(listados);
        }


        //-----------------------------------------------------------------------------------------------------

    }
}