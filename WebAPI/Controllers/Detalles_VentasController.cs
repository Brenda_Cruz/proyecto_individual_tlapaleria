﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using COMMON.Entidades;
using COMMON.Validadores;
using COMMON.Modelos;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Detalles_VentasController : GenericController<Detalles_Venta>
    {
        public Detalles_VentasController() : base(new Detalles_VentasValidator())
        {
        }

        //-----------------------------------------------------------------------------------------------------
        [HttpGet("ObtenerDetalles_VentaCompletos")]
        public ActionResult<List<ModelDetalles_VentaListado>> ObtenerDetalles_VentaCompletos()
        {
            List<Detalles_Venta> detalles_venta = ObtenerDatos<Detalles_Venta>();

            List<ModelDetalles_VentaListado> listados = new List<ModelDetalles_VentaListado>();

            List<Productos> productos = ObtenerDatos<Productos>();
            List<Clientes> clientes = ObtenerDatos<Clientes>();
            List<Usuarios> usuarios = ObtenerDatos<Usuarios>();
            List<Ventas> ventas = ObtenerDatos<Ventas>();

            foreach (var item in detalles_venta)
            {
                listados.Add(new ModelDetalles_VentaListado()
                {
                    Id = item.Id,
                    IdVentas = detalles_venta.SingleOrDefault(c=>c.Id==item.IdVenta)?.IdVenta,
                    NombreProducto = productos.SingleOrDefault(c => c.Id == item.IdProductos)?.Nombre,
                    NombreCliente = productos.SingleOrDefault(c => c.Id == item.IdCliente)?.Nombre,
                    NombreUsuario = usuarios.SingleOrDefault(c => c.Id == item.IdUsuarios)?.Nombre,
                    Cantidad=item.Cantidad,
                    Total=item.Total,
                    Importe=item.Importe,
                    FechaVenta=item.FechaVenta
                });
            }

            return Ok(listados);
        }


        //-----------------------------------------------------------------------------------------------------

    }
}