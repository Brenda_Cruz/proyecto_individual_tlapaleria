﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using COMMON.Entidades;
using COMMON.Validadores;
using COMMON.Modelos;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VentasController : GenericController<Ventas>
    {
        public VentasController(): base(new VentasValidator())
        {
        }

        //-----------------------------------------------------------------------------------------------------
        [HttpGet("ObtenerVentasCompletos")]
        public ActionResult<List<ModelVentasListado>> ObtenerVentasCompletos()
        {
            List<Ventas> ventas = ObtenerDatos<Ventas>();

            List<ModelVentasListado> listados = new List<ModelVentasListado>();

            List<Productos> productos = ObtenerDatos<Productos>();
            List<Clientes> clientes = ObtenerDatos<Clientes>();
            List<Usuarios> usuarios = ObtenerDatos<Usuarios>();

            foreach (var item in ventas)
            {
                listados.Add(new ModelVentasListado()
                {
                    Id = item.Id,
                    IdVentas = item.IdVentas,
                    FechaVenta = item.FechaVenta,
                    IdProducto=item.IdProducto,
                    IdCliente=item.IdCliente,
                    IdUsuario=item.IdUsuario,
                    NombreProducto = productos.SingleOrDefault(c => c.Id == item.IdProducto)?.Nombre,
                    NombreClientes=clientes.SingleOrDefault(c=>c.Id==item.IdCliente)?.Nombre,
                    NombreUsuario=usuarios.SingleOrDefault(c=>c.Id==item.IdUsuario)?.Nombre
                });
            }

            return Ok(listados);
        }


        //-----------------------------------------------------------------------------------------------------

    }
}
