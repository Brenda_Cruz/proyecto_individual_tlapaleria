﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using COMMON.Entidades;
using COMMON.Validadores;
using COMMON.Modelos;
using LiteDB;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : GenericController<Clientes>
    {
        public ClientesController() : base(new ClientesValidator())
        {

        }

        //-----------------------------------------------------------------------------------------------------
        [HttpGet("ObtenerClientesCompletos")]
        public ActionResult<List<ModelClientesListado>> ObtenerClientesCompletos()
        {
            List<Clientes> clientes = ObtenerDatos<Clientes>();

            List<ModelClientesListado> listados = new List<ModelClientesListado>();

            foreach (var item in clientes)
            {
                listados.Add(new ModelClientesListado()
                {
                    Id = item.Id,
                    IdCliente = item.IdCliente,
                    Nombre = item.Nombre,
                    ApellidoPaterno = item.ApellidoPaterno,
                    ApellidoMaterno = item.ApellidoMaterno,
                    Telefono = item.Telefono,
                    Sexo = item.Sexo,
                    Direccion = item.Direccion,
                    Habilitado = item.Habilitado,
                    Password = item.Password,
                    Color = item.Habilitado ? "#00FF00" : "#FF0000" //RGB
                });
            }

            return Ok(listados);
        }


        private string ObtenNombreImagen(string idRol, List<Rol> roles)
        {
            Rol r = roles.SingleOrDefault(i => i.Id == idRol);
            if (r != null)
            {
                switch (r.Nombre)
                {
                    case "cliente":
                        return "cliente.png";
                    case "usuario":
                        return "user.png";
                    default:
                        return "usuario.png";
                }
            }
            else
            {
                return "no.png";
            }
        }

        //
        [HttpPost("LoginCliente/{IdCliente}/{password}")]
        public ActionResult<List<Clientes>> Login(string IdClientes, string password)
        {
            try
            {
                List<Clientes> dato = new List<Clientes>();
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<Clientes>(typeof(Clientes).Name);
                    dato = col.Find(u => u.IdCliente == u.IdCliente && u.Password == u.Password).ToList();

                }
                if (dato != null)
                {
                    return Ok(dato);
                }
                else
                {
                    return BadRequest("Usuario y/o Contraseña incorrecta");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        //-----------------------------------------------------------------------------------------------------

    }
}