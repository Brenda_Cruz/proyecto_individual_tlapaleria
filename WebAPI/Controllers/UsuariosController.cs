﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using COMMON.Entidades;
using COMMON.Validadores;
using COMMON.Modelos;
using LiteDB;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : GenericController<Usuarios>
    {
        public UsuariosController() : base(new UsuariosValidator())
        {

        }

//-----------------------------------------------------------------------------------------------------
        [HttpGet("ObtenerUsuariosCompletos")]
        public ActionResult<List<ModelUsuarioListado>> ObtenerUsuariosCompletos()
        {
            List<Usuarios> usuarios = ObtenerDatos<Usuarios>();

            List<ModelUsuarioListado> listados = new List<ModelUsuarioListado>();

            List<Rol> roles = ObtenerDatos<Rol>();
            foreach (var item in usuarios)
            {
                listados.Add(new ModelUsuarioListado()
                {
                    ApellidoPaterno = item.ApellidoPaterno,
                    ApellidoMaterno = item.ApellidoMaterno,
                    Habilitado = item.Habilitado,
                    Id = item.Id,
                    IdRol = item.IdRol,
                    IdUsuario = item.IdUsuario,
                    Nombre = item.Nombre,
                    Password = item.Password,
                    Imagen = ObtenNombreImagen(item.IdRol, roles),
                    NombreRol=roles.SingleOrDefault(c=>c.Id==item.IdRol)?.Nombre,
                    Color = item.Habilitado ? "#00FF00" : "#FF0000" //RGB
                });
            }

            return Ok(listados);
        }

        private string ObtenNombreImagen(string idRol, List<Rol> roles)
        {
            Rol r = roles.SingleOrDefault(i => i.Id == idRol);
            if (r != null)
            {
                switch (r.Nombre)
                {
                    case "cliente":
                        return "cliente.png";
                    case "usuario":
                        return "user.png";
                    default:
                        return "usuario.png";
                }
            }
            else
            {
                return "no.png";
            }
        }

        [HttpGet("LoginUsuarios/{idusuario}/{password}")]
        public ActionResult<List<Usuarios>> LoginUsuarios(string idusuario, string password)
        {
            try
            {
                List<Usuarios> datos = new List<Usuarios>();
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<Usuarios>(typeof(Usuarios).Name);
                    datos = col.Find(u => u.IdUsuario == idusuario && u.Password == password).ToList();
                }
                return Ok(datos);
            }
            catch (Exception)
            {
                return BadRequest("Usuario y/o Contraseña incorrecta");
            }
        }


        //-----------------------------------------------------------------------------------------------------

    }
}

/*
[HttpPost("Login")]
        public ActionResult<Usuarios> Login([FromBody] Login login)
        {
            try
            {
                Usuarios dato;
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<Usuarios>(typeof(Usuarios).Name);
                    dato = col.Find(u => u.Id == login.IdUsuario && u.Password == login.Password && u.Habilitado).SingleOrDefault();
                }
                if (dato != null)
                {
                    return Ok(dato);
                }
                else
                {
                    return BadRequest("Usuario y/o Contraseña incorrecta");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        } 

 */