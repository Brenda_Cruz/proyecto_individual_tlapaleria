﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarcaController : GenericController<Marca>
    {
        public MarcaController() : base(new MarcaValidator())
        {

        }


        //-----------------------------------------------------------------------------------------------------
        [HttpGet("ObtenerMarcaCompletos")]
        public ActionResult<List<ModelMarcaListado>> ObtenerMarcaCompletos()
        {
            List<Marca> marcas = ObtenerDatos<Marca>();

            List<ModelMarcaListado> listados = new List<ModelMarcaListado>();

            foreach (var item in marcas)
            {
                listados.Add(new ModelMarcaListado()
                {
                    Id = item.Id,
                    Nombre = item.Nombre,
                    Imagen = ObtenNombreImagen(item.Id, marcas)
                });
            }

            return Ok(listados);
        }

        private string ObtenNombreImagen(string id, List<Marca> marcas)
        {
            Marca r = marcas.SingleOrDefault(i => i.Id == id);
            if (r != null)
            {
                switch (r.Nombre)
                {
                    case "TRUPER":
                        return "truper.png";
                    case "PRETUL":
                        return "pretul.png";
                    case "HERMEX":
                        return "hermex.png";
                    default:
                        return "trabajos.png";
                }
            }
            else
            {
                return "no.png";
            }
        }

    }
}
