﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ.Managers
{
    public class RolManager : GenericManager<Rol>
    {
        public RolManager(GenericValidator<Rol> validator) : base(validator)
        {
        }

        public List<ModelRolListado> ObtenerTodosListados() => ObtenerTodosListadosAsync().Result;

        private async Task<List<ModelRolListado>> ObtenerTodosListadosAsync()
        {
            HttpResponseMessage response = await client.GetAsync("Rol/ObtenerRolesCompletos").ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                Error = "";
                return JsonConvert.DeserializeObject<List<ModelRolListado>>(content);
            }
            else
            {
                Error = content;
                return null;
            }
        }

    }
}
