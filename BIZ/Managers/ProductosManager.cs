﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ.Managers
{
    public class ProductosManager : GenericManager<Productos>
    {
        public ProductosManager(GenericValidator<Productos> validator) : base(validator)
        {
        }

        public List<ModelProductosListado> ObtenerTodosListados() => ObtenerTodosListadosAsync().Result;

        private async Task<List<ModelProductosListado>> ObtenerTodosListadosAsync()
        {
            HttpResponseMessage response = await client.GetAsync("Productos/ObtenerProductosCompletos").ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                Error = "";
                return JsonConvert.DeserializeObject<List<ModelProductosListado>>(content);
            }
            else
            {
                Error = content;
                return null;
            }
        }

    }
}
