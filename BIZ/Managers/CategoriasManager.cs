﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ.Managers
{
    public class CategoriasManager:GenericManager<Categorias>
    {
        public CategoriasManager(GenericValidator<Categorias> validator) : base(validator)
        {
        }

        public List<ModelCategoriasListado> ObtenerTodosListados() => ObtenerTodosListadosAsync().Result;

        private async Task<List<ModelCategoriasListado>> ObtenerTodosListadosAsync()
        {
            HttpResponseMessage response = await client.GetAsync("Categorias/ObtenerCategoriasCompletos").ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                Error = "";
                return JsonConvert.DeserializeObject<List<ModelCategoriasListado>>(content);
            }
            else
            {
                Error = content;
                return null;
            }
        }

    }
}
