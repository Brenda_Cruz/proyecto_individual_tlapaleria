﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ.Managers
{
    public class MarcaManager:GenericManager<Marca>
    {
        public MarcaManager(GenericValidator<Marca> validator) : base(validator)
        {

        }

        public List<ModelMarcaListado> ObtenerTodosListados() => ObtenerTodosListadosAsync().Result;

        private async Task<List<ModelMarcaListado>> ObtenerTodosListadosAsync()
        {
            HttpResponseMessage response = await client.GetAsync("Marca/ObtenerMarcaCompletos").ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                Error = "";
                return JsonConvert.DeserializeObject<List<ModelMarcaListado>>(content);
            }
            else
            {
                Error = content;
                return null;
            }
        }

    }
}