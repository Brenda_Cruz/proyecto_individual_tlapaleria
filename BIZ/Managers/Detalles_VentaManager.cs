﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ.Managers
{
    public class Detalles_VentaManager : GenericManager<Detalles_Venta>
    {
        public Detalles_VentaManager(GenericValidator<Detalles_Venta> validator) : base(validator)
        {
        }

        public List<ModelDetalles_VentaListado> ObtenerTodosListados() => ObtenerTodosListadosAsync().Result;

        private async Task<List<ModelDetalles_VentaListado>> ObtenerTodosListadosAsync()
        {
            HttpResponseMessage response = await client.GetAsync("Detalles_Venta/ObtenrDetalles_VentaCompletos").ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                Error = "";
                return JsonConvert.DeserializeObject<List<ModelDetalles_VentaListado>>(content);
            }
            else
            {
                Error = content;
                return null;
            }
        }

        public Detalles_Venta Login(Login login) => LoginAsync(login).Result;


        private async Task<Detalles_Venta> LoginAsync(Login login)
        {
            var body = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync("Detalles_Venta/Login", body).ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                Error = "";
                return JsonConvert.DeserializeObject<Detalles_Venta>(content);
            }
            else
            {
                Error = content;
                return null;
            }
        }
    }
}
