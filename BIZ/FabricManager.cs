﻿using BIZ.Managers;
using COMMON.Entidades;
using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public static class FabricManager
    {
        public static VentasManager VentasManager() => new VentasManager(new VentasValidator());
        public static ClientesManager ClientesManager() => new ClientesManager(new ClientesValidator());
        public static ProductosManager ProductosManager() => new ProductosManager(new ProductosValidator());

        public static Detalles_VentaManager Detalles_VentaManager() => new Detalles_VentaManager(new Detalles_VentasValidator());
        public static RolManager RolManager() => new RolManager(new RolValidator());
        public static UsuariosManager UsuariosManager() => new UsuariosManager(new UsuariosValidator());
        public static MarcaManager MarcaManager() => new MarcaManager(new MarcaValidator());
        public static CategoriasManager CategoriasManager() => new CategoriasManager(new CategoriasValidator());

    }
}
