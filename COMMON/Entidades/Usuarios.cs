﻿namespace COMMON.Entidades
{
    public class Usuarios : Base
    {
        //empleado
        // public string Id {get;set;}
        public string IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        // public string Telefono { get; set; }
        // public string Sexo { get; set; }
        //  public string Direccion { get; set; }
        //  public DateTime FechaIngreso { get; set; }
        // public string UserName { get; set; }
        public string Password { get; set; }
      //  public double Salario { get; set; }
        public string IdRol { get; set; }
        public bool Habilitado { get; set; }
       

        public override string ToString()
        {
            return $"{IdUsuario}-{ApellidoPaterno} {ApellidoMaterno} {Nombre} {IdRol}";
        }
    }
}
