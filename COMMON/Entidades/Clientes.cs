﻿namespace COMMON.Entidades
{
    public class Clientes : Base
    {
        //public string Id { get; set; }
        public string IdCliente { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Telefono { get; set; }
        public string Sexo { get; set; }
        public string Direccion { get; set; }
        public bool Habilitado { get; set; }
        // public string IdRol { get; set; }
        public string Password { get; set; }

        public override string ToString()
        {
            return $"{IdCliente}-{ApellidoPaterno} {ApellidoMaterno} {Nombre} {Telefono}";
        }
       
    }
}
