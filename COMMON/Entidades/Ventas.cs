﻿namespace COMMON.Entidades
{
    public class Ventas:Base
    {
       // public string Id { get; set; }
        public string IdVentas { get; set; }
        public string IdProducto { get; set; }
        public DateTime? FechaVenta { get; set; }
        public string IdCliente { get; set; }
        public string IdUsuario { get; set; }
        public override string ToString()
        {
            return $"{IdVentas}";
        }
    }
}
