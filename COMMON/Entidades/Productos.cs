﻿namespace COMMON.Entidades
{
    public class Productos:Base
    {
        // public string Id { get; set; }
        public string IdProducto { get; set; }
        public string Nombre { get; set; }
        public double Costo { get; set; }
        public int Cantidad { get; set; }
        public DateTime? FechaIngreso{ get; set; }
        public string IdMarca { get; set; }
        public string IdCategoria { get; set; }
        

        public override string ToString()
        {
            return $"{IdProducto}";
        }
    }
}
