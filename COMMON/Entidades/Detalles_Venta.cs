﻿namespace COMMON.Entidades
{
    public class Detalles_Venta:Base
    {
       // public string Id { get; set; }
        public string IdVenta { get; set; }
        public string IdProductos { get; set; }
        public string IdCliente { get; set; }
        public string IdUsuarios { get; set; }
        public int Cantidad { get; set; }
        public double Total { get; set; }
        public double Importe { get; set; }
        public DateTime? FechaVenta { get; set; }

        public override string ToString()
        {
            return $"{IdVenta}";
        }
    }
}
