﻿using COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Modelos
{
    public class ModelDetalles_VentaListado:Detalles_Venta
    {
        public string Imagen { get; set; }
        public string Nombre { get; set; }
        public string Color { get; set; }
        public string NombreProducto { get; set; }
        public string NombreUsuario { get; set; }
        public string NombreCliente { get; set; }
        public string IdVentas { get; set; }
    }
}
