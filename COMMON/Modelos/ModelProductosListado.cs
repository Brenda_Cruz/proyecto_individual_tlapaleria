﻿using COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Modelos
{
    public class ModelProductosListado:Productos
    {
        //public string NombreProducto { get; set; }
        public string NombreMarcas { get; set; }
        public string NombreCategorias { get; set; }
    }
}
