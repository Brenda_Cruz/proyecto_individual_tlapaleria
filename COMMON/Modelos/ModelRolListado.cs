﻿using COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Modelos
{
    public class ModelRolListado: Rol
    {
        public string NombreRol { get; set; }
        public string Imagen { get; set; }
        public string Color { get; set; }
    }
}
