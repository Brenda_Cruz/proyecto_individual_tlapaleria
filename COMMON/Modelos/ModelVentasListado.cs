﻿using COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Modelos
{
    public class ModelVentasListado:Ventas
    {
        //public string IdProducto { get; set; }
        //public string IdUsuario { get; set; }
        //public string IdCliente { get; set; }
        //public string IdVentas { get; set; }
        public string NombreProducto { get; set; }
        public string NombreUsuario { get; set; }
        public string NombreClientes { get; set; }
    }
}
