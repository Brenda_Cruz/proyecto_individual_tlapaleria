﻿using FluentValidation;
using COMMON.Entidades;

namespace COMMON.Validadores
{
    public class UsuariosValidator:GenericValidator<Usuarios>
    {
        public UsuariosValidator()
        {
            //RuleFor(c => c.Id).NotEmpty();
            RuleFor(u => u.IdUsuario).NotEmpty().MaximumLength(10);
            RuleFor(u => u.Nombre).NotEmpty().MaximumLength(30);
            RuleFor(u => u.ApellidoPaterno).NotEmpty().MaximumLength(30);
            RuleFor(u => u.ApellidoMaterno).NotEmpty().MaximumLength(30);
            //RuleFor(c => c.Telefono).NotEmpty().MaximumLength(30);
            //RuleFor(c => c.Sexo).NotEmpty().MaximumLength(30);
            //RuleFor(c => c.Direccion).NotEmpty().MaximumLength(30);
            //RuleFor(c => c.FechaIngreso).LessThan(DateTime.Now).WithMessage("La venta no puede ser a futuro");
            // RuleFor(u => u.UserName).NotEmpty();
            RuleFor(u => u.Password).NotEmpty();
            //RuleFor(c => c.Salario).NotEmpty();
            RuleFor(u => u.IdRol).NotEmpty();
            //RuleFor(u => u.Habilitado).NotEmpty();
        }
    }
}
