﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class MarcaValidator:GenericValidator<Marca>
    {
        public MarcaValidator()
        {
           // RuleFor(r => r.IdMarca).NotEmpty();
            RuleFor(r => r.Nombre).NotEmpty().MaximumLength(50);
        }
    }
}
