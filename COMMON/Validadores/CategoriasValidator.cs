﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class CategoriasValidator:GenericValidator<Categorias>
    {
        public CategoriasValidator()
        {
            //RuleFor(c => c.IdCategorias).NotEmpty();
            RuleFor(c => c.Nombre).NotEmpty().MaximumLength(50);
        }
    }
}
