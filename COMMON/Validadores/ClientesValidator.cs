﻿using FluentValidation;
using COMMON.Entidades;

namespace COMMON.Validadores
{
    public class ClientesValidator : GenericValidator<Clientes>
    {
        public ClientesValidator()
        {
            //RuleFor(c => c.Id).NotEmpty();
            RuleFor(u => u.IdCliente).NotEmpty().MaximumLength(10);
            RuleFor(c => c.Nombre).NotEmpty().MaximumLength(30);
            RuleFor(c => c.ApellidoPaterno).NotEmpty().MaximumLength(30);
            RuleFor(c => c.ApellidoMaterno).NotEmpty().MaximumLength(30);
            RuleFor(c => c.Telefono).NotEmpty().MaximumLength(30);
            RuleFor(c => c.Sexo).NotEmpty().MaximumLength(30);
            RuleFor(c => c.Direccion).NotEmpty().MaximumLength(30);
            RuleFor(u => u.Habilitado).NotEmpty();
            //RuleFor(u => u.IdRol).NotEmpty();
            RuleFor(u => u.Password).NotEmpty();
        }
    }
}