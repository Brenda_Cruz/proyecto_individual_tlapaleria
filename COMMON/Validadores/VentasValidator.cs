﻿using FluentValidation;
using COMMON.Entidades;

namespace COMMON.Validadores
{
    public class VentasValidator : GenericValidator<Ventas>
    {
        public VentasValidator()
        {
            //RuleFor(c => c.Id).NotEmpty();
            RuleFor(c => c.IdVentas).NotEmpty().MaximumLength(10);
            RuleFor(c => c.IdProducto).NotEmpty();
            RuleFor(v => v.FechaVenta).NotEmpty();
            RuleFor(c => c.IdCliente).NotEmpty();
            RuleFor(c => c.IdUsuario).NotEmpty();
        }
    }
}
