﻿using FluentValidation;
using COMMON.Entidades;

namespace COMMON.Validadores
{
    public class Detalles_VentasValidator : GenericValidator<Detalles_Venta>
    {
        public Detalles_VentasValidator()
        {
            //RuleFor(c => c.Id).NotEmpty();
            RuleFor(c => c.IdVenta).NotEmpty().MaximumLength(10);
            RuleFor(c => c.IdProductos).NotEmpty().MaximumLength(10);
            RuleFor(c => c.IdCliente).NotEmpty().MaximumLength(10);
            RuleFor(c => c.IdUsuarios).NotEmpty().MaximumLength(10);
            RuleFor(c => c.Cantidad).NotEmpty();
            RuleFor(c => c.Total).NotEmpty();
            RuleFor(c => c.Importe).NotEmpty();
            RuleFor(v => v.FechaVenta).NotEmpty();
            //RuleFor(v => v.FechaVenta).LessThan(DateTime.Now).WithMessage("La venta no puede ser a futuro");
        }
    }
}
