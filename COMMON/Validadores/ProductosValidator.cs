﻿using FluentValidation;
using COMMON.Entidades;

namespace COMMON.Validadores
{
    public class ProductosValidator : GenericValidator<Productos>
    {
        public ProductosValidator()
        {
            //RuleFor(c => c.Id).NotEmpty();
            RuleFor(u => u.IdProducto).NotEmpty().MaximumLength(10);
            RuleFor(c => c.Nombre).NotEmpty().MaximumLength(30);
            RuleFor(c => c.Costo).NotEmpty();
            RuleFor(c => c.Cantidad).NotEmpty();
            RuleFor(c => c.FechaIngreso).NotEmpty();
            RuleFor(c => c.IdMarca).NotEmpty();
            RuleFor(c => c.IdCategoria).NotEmpty();

        }
    }
}
